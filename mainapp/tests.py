from django.test import TestCase, Client
from django.urls import resolve

from .views import index

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

import time

# Create your tests here.
class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)

    def test_if_url_exist_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_page_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = Options()
        options.headless = True
        cls.selenium = webdriver.Firefox(options=options)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_change_color_button(self):
        self.selenium.get(self.live_server_url)

        color = self.selenium.find_element_by_tag_name('body').value_of_css_property("color")
        self.assertEqual("rgb(255, 255, 255)", color)

        self.selenium.find_element_by_id('change_theme_button').click()
        time.sleep(1)
        color = self.selenium.find_element_by_tag_name('body').value_of_css_property("color")
        self.assertEqual("rgb(0, 0, 0)", color)

        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)

        self.selenium.find_element_by_class_name("accordion-header").click()
        time.sleep(1)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("block", display)

        self.selenium.find_element_by_class_name("accordion-header").click()
        time.sleep(1)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)


